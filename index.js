const express = require('express');
const mariadb = require('mariadb');
const cors = require('cors');
const app = express();
app.use(cors());
const bodyParser = require('body-parser');
const dbConfig = require('./mariadb.js');



const pool = mariadb.createPool(dbConfig);



// Set up body parser middleware to parse request bodies
app.use(bodyParser.json());

// Handle POST requests to /save-string
app.post('/save-string', (request, response) => {
  // Extract the string from the request body
  const reviewText = request.body.Text;

  // logic to save the string to a database 

  const putReview = `insert into text_table (review) values (?)`;

  pool.getConnection()
    .then(connection => {
        connection.query(putReview, [reviewText], (error, results) => {
            if (error) {
              console.error('Error saving string to MariaDB database: ', error);
              response.status(500).send('Error saving string to MariaDB database');
              return;
            }
            console.log('String saved to MariaDB database');
            response.send('String saved to MariaDB database');
          }).catch(error => {
            console.error('Error saving string to MariaDB database: ', error);
            response.status(500).send('Error saving string to MariaDB database');
          });
    })
    .catch(error => {
      console.error('Error connecting to MariaDB server: ', error);
      response.status(500).send('Error connecting to MariaDB server');
    });
    
  // Recived string printing
  console.log(reviewText);
  // Send a response to the client
  response.send('String saved successfully!');
});


// Retrive the messages stored in server

app.get('/reviews', async (req, res) => {
  let connection;
  
  try {
    connection = await pool.getConnection();
    
    const getReview = 'SELECT id,review FROM text_table';
    const result = await connection.query(getReview);
    
    //const columnData = result.map(row => row.review);
    res.json(result);
  } catch (err) {
    console.error('Error fetching data: ' + err.stack);
    res.status(500).send('Error fetching data from database');
  } finally {
    if (connection) {
      connection.release();
    }
  }
});

//delete message entry

app.delete('/delete/:id', async (req, res) => {
  const id = req.params.id;

  try {
    const connection = await pool.getConnection();
    await connection.query('DELETE FROM  text_table where id = ?', [id]);
    connection.release();
    res.status(200).send(`Entry with ID ${id} deleted successfully.`);
  } catch (err) {
    console.error(err);
    res.status(500).send('Error deleting the entry.');
  }
});



app.post('/login', async (req, res) => {
  const { username, password } = req.body;

  try {
    const conn = await pool.getConnection();
    const result = await conn.query(
      `SELECT * FROM users WHERE username = '${username}' AND password = '${password}'`
    );
    conn.release();

    if (result.length > 0) {
      return res.status(200).json({ message: 'Login successful' });
    } else {
      return res.status(401).json({ message: 'Invalid username or password' });
    }
  } catch (err) {
    console.error(err);
    return res.status(500).json({ message: 'Internal server error' });
  }
});


// Start the server
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});
